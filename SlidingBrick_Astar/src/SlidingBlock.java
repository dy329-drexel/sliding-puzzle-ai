
public class SlidingBlock {

	public int dx;
	public	int dx2;
	public int dy;
	public int dy2;

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDx2() {
		return dx2;
	}

	public void setDx2(int dx2) {
		this.dx2 = dx2;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}

	public int getDy2() {
		return dy2;
	}

	public void setDy2(int dy2) {
		this.dy2 = dy2;
	}

	public SlidingBlock(int dx, int dy, int dx2, int dy2) {

		this.dx = dx;
		this.dx2 = dx2;
		this.dy = dy;
		this.dy2 = dy2;
	}

	public SlidingBlock() {
		// TODO Auto-generated constructor stub
	}

	public  SlidingBlock getBlockDimension(State board, int peice) {
		// Dimensions of the piece
		int x1 = -1, y1 = -1;
		int x2 = -1, y2 = -1;
//find the coordinates and return the block
		for (int i = 0; i < board.x; i++) {

			for (int j = 0; j < board.y; j++) {

				if (board.st[i][j] == peice) {

					if (x1 == -1 && y1 == -1) {
						x1 = i;
						y1 = j;
						x2 = i;
						y2 = j;
					} else {

						x2 = i;
						y2 = j;
					}
				}
			}

			if (x1 == -1 && i == board.x - 1) {
				return null;
			}
		}
		dx=x1;
		dx2=x2;
		dy=y1;
		dy2=y2;

		return new SlidingBlock(x1, y1, x2, y2);

	}
	public int getMinDistance(SlidingBlock b) {
		
	
		// get minimum distance
	int mindist=-1;
	int dist;
//find the coordinates and return the block
	dist=Math.abs(dx-b.dx)+ Math.abs(dy-b.dy);
	mindist=dist;
	dist=Math.abs(dx-b.dx)+ Math.abs(dy2-b.dy2);

	if(dist<mindist)
		mindist=dist;
	dist=Math.abs(dx2-b.dx2)+ Math.abs(dy-b.dy);

	if(dist<mindist)
		mindist=dist;
	dist=Math.abs(dx2-b.dx2)+ Math.abs(dy2-b.dy2);

	if(dist<mindist)
		mindist=dist;
	
	
	
	
	return mindist;

	}
	public  SlidingBlock getGoalDimension(State board, int peice) {
		// Dimensions of the piece
		int x1 = -1, y1 = -1;
		int x2 = -1, y2 = -1;
//find the coordinates and return the block
		for (int i = 0; i < board.x; i++) {

			for (int j = 0; j < board.y; j++) {

				if (board.st[i][j] == peice) {

					if (x1 == -1 && y1 == -1) {
						x1 = i;
						y1 = j;
						x2 = i;
						y2 = j;
					} else {

						x2 = i;
						y2 = j;
					}
				}
			}

			
		}
		dx=x1;
		dx2=x2;
		dy=y1;
		dy2=y2;

		return new SlidingBlock(x1, y1, x2, y2);

	}
public void print(){
	System.out.println("x1:"+dx+"y1:"+dy+"other "+"x1:"+dx2+"y1:"+dy2);
}
}
