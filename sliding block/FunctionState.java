import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FunctionState {

	// get list of available moves.
	public ArrayList<Move> getMoveSet(State board, int piece) {
		ArrayList<Move> moves = null;

		if (board.st != null) {

			SlidingBlock block = SlidingBlock.getBlockDimension(board, piece);

			if (block == null)
				return null;

			moves = new ArrayList<Move>();

			// check left
			if (checkMove(board.st, "left", block))
				moves.add(new Move(piece, "left"));
			// check right
			if (checkMove(board.st, "right", block))
				moves.add(new Move(piece, "right"));
			// check up side
			if (checkMove(board.st, "up", block))
				moves.add(new Move(piece, "up"));
			// check down side
			if (checkMove(board.st, "down", block))
				moves.add(new Move(piece, "down"));
		}
		return moves;
	}

	// check if a piece can moves
	private boolean checkMove(int[][] board, String dir, SlidingBlock ablock) {
		int x1 = ablock.getDx();
		int x2 = ablock.getDx2();
		int y1 = ablock.getDy();
		int y2 = ablock.getDy2();
		boolean specialBlock = false;
		if (board[x1][y1] == 2)
			specialBlock = true;
		int piece = board[x1][y1];

		boolean move = true;
		// check the respective direction
		if (dir.equals("up")) {
			x1 += -1;
			x2 += -1;
			y1 += 0;
			y2 += 0;
		} else if (dir.equals("right")) {
			x1 += 0;
			x2 += 0;
			y1 += 1;
			y2 += 1;
		} else if (dir.equals("left")) {
			x1 += 0;
			x2 += 0;
			y1 += -1;
			y2 += -1;
		} else if (dir.equals("down")) {
			x1 += 1;
			x2 += 1;
			y1 += 0;
			y2 += 0;
		} else {
			x1 += 0;
			x2 += 0;
			y1 += 0;
			y2 += 0;
		}

		for (int i = x1; i <= x2; i++) {
			for (int j = y1; j <= y2; j++) {
				if (specialBlock) {
					if (board[i][j] != 0 && board[i][j] != -1 && board[i][j] != piece) {
						return false;

					}
				} else {
					if (board[i][j] != 0 && board[i][j] != piece) {
						return false;

					}
				}

			}
		}

		return move;
	}

	// get list of nodes from a node
	public ArrayList<SearchNode> getAllMoves(SearchNode ns) {
		ArrayList<SearchNode> allmoves = new ArrayList<SearchNode>();

		State board = ns.s;

		// search for a move for all peices and add them to search node list
		for (int i = 0; i < board.x; i++) {

			for (int j = 0; j < board.y; j++) {
				int positionValue = board.st[i][j];

				if (positionValue != -1 && positionValue != 0 && positionValue != 1) {
					ArrayList<Move> moves = getMoveSet(board, positionValue);

					if (moves.size() != 0) {
						for (int k = 0; k < moves.size(); k++) {
							SearchNode a = new SearchNode(
									(applyMoveCloning(board, moves.get(k).direction, moves.get(k).piece)), ns,
									moves.get(k).direction, moves.get(k).piece);
							allmoves.add(a);
						}
					}

				}
			}
		}

		return allmoves;

	}

	// apply move
	public boolean applyMove(State board, String direction, int piece) {

		SlidingBlock block = SlidingBlock.getBlockDimension(board, piece);

		changeBlockPosition(board.st, direction, piece, block);

		return true;
	}

	// make a move and swap position on board
	private void changeBlockPosition(int[][] board, String dir, int value, SlidingBlock block) {
		int x1 = block.getDx();
		int x2 = block.getDx2();
		int y1 = block.getDy();
		int y2 = block.getDy2();

		for (int i = x1; i <= x2; i++) {
			for (int j = y1; j <= y2; j++) {
				board[i][j] = 0;
			}
		}

		if (dir.equals("up")) {
			x1 += -1;
			x2 += -1;
			y1 += 0;
			y2 += 0;
		} else if (dir.equals("right")) {
			x1 += 0;
			x2 += 0;
			y1 += 1;
			y2 += 1;
		} else if (dir.equals("left")) {
			x1 += 0;
			x2 += 0;
			y1 += -1;
			y2 += -1;
		} else if (dir.equals("down")) {
			x1 += 1;
			x2 += 1;
			y1 += 0;
			y2 += 0;
		} else {
			x1 += 0;
			x2 += 0;
			y1 += 0;
			y2 += 0;
		}

		for (int i = x1; i <= x2; i++) {
			for (int j = y1; j <= y2; j++) {
				board[i][j] = value;
			}
		}

	}

	// return a new state of the board with the move
	public State applyMoveCloning(State board, String dir, int piece) {
		State b = (State) cloneState(board);

		if (applyMove(b, dir, piece))
			return b;
		else
			return null;

	}

	// clone a object
	public Object cloneState(State board) {
		State b = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(board);
			oos.flush();

			ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bin);
			b = (State) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return b;

	}

}
