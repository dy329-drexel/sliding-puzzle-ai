import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class State implements java.io.Serializable {
	int x;
	int y;
	int[][] st;

	// constructor
	public State(int a, int b, int[][] array) {

		x = a;
		y = b;
		st = new int[x][y];
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				st[i][j] = array[i][j];
			}

		}
	}

	public State() {
		// TODO Auto-generated constructor stub
	}

	// display state function
	public void displayState() {
		System.out.println(y + "," + x);
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {

				System.out.print(st[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("----------------------------------");
	}

	// clone state

	public Object cloneState(State board) {
		State b = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(board);
			oos.flush();

			ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bin);
			b = (State) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return b;

	}
	// return (State) a.clone();
	// State b = new State();
	// b.x = a.x;
	// b.y = a.y;
	// b.st = a.st;
	// return b;

	// check if its complete
	public boolean isComplete(State a) {
		for (int i = 0; i < a.x; i++) {
			for (int j = 0; j < a.y; j++) {
				if (a.st[i][j] == -1)
					return false;
			}

		}
		return true;

	}

	// check is its equal
	public boolean isEqual(State a, State b) {
		if (a.x == b.x && a.y == b.y) {
			for (int i = 0; i < a.x; i++) {
				for (int j = 0; j < a.y; j++) {
					if (a.st[i][j] != b.st[i][j])
						return false;
				}

			}
		}
		return true;
	}

	// normalize and swap function
	public void normalize() {

		int nextIdx = 3;
		for (int i = 0; i < st.length; i++) {
			for (int j = 0; j < st[0].length; j++) {
				if (st[i][j] == nextIdx) {
					nextIdx++;
				} else if (st[i][j] > nextIdx) {
					swapIdx(nextIdx, st[i][j]);
					nextIdx++;
				}
			}
		}

	}

	private void swapIdx(int idx1, int idx2) {
		for (int i = 0; i < st.length; i++) {
			for (int j = 0; j < st[0].length; j++) {
				if (st[i][j] == idx1) {
					st[i][j] = idx2;
				} else if (st[i][j] == idx2) {
					st[i][j] = idx1;
				}
			}
		}
	}

	// getting arraylist of all moves
	public ArrayList<Move> getAllMoves(int aPiece) {
		ArrayList<Move> moves = new ArrayList<Move>();
		if (aPiece < 2)
			return moves; // invalid piece no moves

		boolean pfound = false;
		boolean up = true, down = true, left = true, right = true;

		for (int i = 0; i < x; i++) {
			if ((!up) && (!down) && (!left) && (!right))
				break;

			for (int j = 0; j < y; j++) {
				if ((!up) && (!down) && (!left) && (!right))
					break;

				if (st[i][j] == aPiece) {
					pfound = true;
					// System.out.println("piece found "+aPiece);

					// check for all directions
					if ((st[i - 1][j] != 0) && (st[i - 1][j] != aPiece)) {
						if (aPiece == 2) {
							if (st[i - 1][j] != -1)
								up = false;
						} else
							up = false;

					}

					if ((st[i + 1][j] != 0) && (st[i + 1][j] != aPiece)) {
						if (aPiece == 2) {
							if (st[i + 1][j] != -1)
								down = false;
						} else
							down = false;

					}
					if ((st[i][j - 1] != 0) && (st[i][j - 1] != aPiece)) {
						if (aPiece == 2) {
							if (st[i][j - 1] != -1)
								left = false;
						} else
							left = false;

					}
					if ((st[i][j + 1] != 0) && (st[i][j + 1] != aPiece)) {
						if (aPiece == 2) {
							if (st[i][j + 1] != -1)
								right = false;
						} else
							right = false;

					}

				}

			}

		}
		if (!pfound) {
			return moves;
		}
		if ((!up) && (!down) && (!left) && (!right)) {

			return moves;
		}
		if (up)
			moves.add(new Move(aPiece, "up"));
		if (down)
			moves.add(new Move(aPiece, "down"));
		if (left)
			moves.add(new Move(aPiece, "left"));
		if (right)
			moves.add(new Move(aPiece, "right"));

		return moves;

	}

	public ArrayList<ArrayList<Move>> getAllMoves() {
		int i, j;
		ArrayList<ArrayList<Move>> moves = new ArrayList<ArrayList<Move>>();
		// iterate through all pieces and find moves for them

		ArrayList<Integer> checked = new ArrayList<Integer>();

		for (i = 0; i < x; i++) {
			ArrayList<Move> move = new ArrayList<Move>();
			for (j = 0; j < y; j++) {
				// checked.add(x);

				if (!checked.contains(x)) {
					int x = st[i][j];
					move = getAllMoves(x);
				}

				if (move.size() != 0) {

					moves.add(move);
				}
			}
		}

		return moves;
	}

	public void applyMove(Move amove) {
		int piece = amove.piece;
		String dir = amove.direction;
		int startx = -1, starty = -1;
		// get piece dimension

		for (int i = 0; i < st.length; i++) {
			int[] row = st[i];
			for (int j = 0; j < row.length; j++) {
				if (row[j] == piece) {

					// get coordinate of the piece
					startx = j;
					starty = i;
					break;

				}

			}
		}

		int width = 0, height = 0;
		for (int y = starty; y < st.length; y++) {
			if (st[y][startx] == piece) {
				height++;
			} else {
				break;
			}
		}
		for (int x = startx; x < st[0].length; x++) {
			if (st[starty][x] == piece) {
				width++;

			} else {
				break;
			}
		}
		// make a move
		if (dir.equals("up")) {

			for (int x = startx; x < startx + width; x++) {
				st[starty - 1][x] = piece;
				st[starty + height - 1][x] = 0;
			}
		} else if (dir.equals("down")) {
			for (int x = startx; x < startx + width; x++) {
				st[starty + height][x] = piece;
				st[starty][x] = 0;
			}
		} else if (dir.equals("left")) {
			for (int y = starty; y < starty + height; y++) {
				st[y][startx - 1] = piece;
				st[y][startx + width - 1] = 0;

			}
		} else if (dir.equals("right")) {
			for (int y = starty; y < starty + height; y++) {
				st[y][startx + width] = piece;
				st[y][startx] = 0;

			}
		} else {
			System.out.println("error in applying move");

		}

	}

	public State applyMovesCloning(Move amove) {
		State newState = (State) cloneState(this);
		newState.applyMove(amove);
		return newState;

	}

	public ArrayList<SearchNode> getAllNodes(SearchNode sn, int aPiece) {

		ArrayList<SearchNode> sNodes = new ArrayList<SearchNode>();
		ArrayList<Move> moves = new ArrayList<Move>();
		if (aPiece < 2)
			return null; // invalid piece no moves

		boolean pfound = false;
		boolean up = true, down = true, left = true, right = true;

		for (int i = 0; i < x; i++) {
			if ((!up) && (!down) && (!left) && (!right))
				break;

			for (int j = 0; j < y; j++) {
				if ((!up) && (!down) && (!left) && (!right))
					break;

				if (st[i][j] == aPiece) {
					pfound = true;
					// System.out.println("piece found "+aPiece);

					// check for all directions
					if ((st[i - 1][j] != 0) && (st[i - 1][j] != aPiece)) {
						if (aPiece == 2) {
							if (st[i - 1][j] != -1)
								up = false;
						} else
							up = false;

					}

					if ((st[i + 1][j] != 0) && (st[i + 1][j] != aPiece)) {
						if (aPiece == 2) {
							if (st[i + 1][j] != -1)
								down = false;
						} else
							down = false;

					}
					if ((st[i][j - 1] != 0) && (st[i][j - 1] != aPiece)) {
						if (aPiece == 2) {
							if (st[i][j - 1] != -1)
								left = false;
						} else
							left = false;

					}
					if ((st[i][j + 1] != 0) && (st[i][j + 1] != aPiece)) {
						if (aPiece == 2) {
							if (st[i][j + 1] != -1)
								right = false;
						} else
							right = false;

					}

				}

			}

		}
		if (!pfound) {
			return sNodes;
		}
		if ((!up) && (!down) && (!left) && (!right)) {

			return sNodes;
		}
		if (up)
			moves.add(new Move(aPiece, "up"));
		if (down)
			moves.add(new Move(aPiece, "down"));
		if (left)
			moves.add(new Move(aPiece, "left"));
		if (right)
			moves.add(new Move(aPiece, "right"));

		for (int i = 0; i < moves.size(); i++) {
			sNodes.add(new SearchNode(this.applyMovesCloning(moves.get(i)), sn, moves.get(i).direction, aPiece));

		}

		return sNodes;

	}

	public ArrayList<ArrayList<SearchNode>> getAllNodes(SearchNode current) {
		int i, j;
		this.st = current.s.st;
		ArrayList<ArrayList<SearchNode>> moves = new ArrayList<ArrayList<SearchNode>>();
		// iterate through all pieces and find moves for them

		ArrayList<Integer> checked = new ArrayList<Integer>();

		for (i = 0; i < x; i++) {
			ArrayList<SearchNode> move = new ArrayList<SearchNode>();
			for (j = 0; j < y; j++) {
				// checked.add(x);

				if (!checked.contains(x)) {
					int x = st[i][j];
					move = getAllNodes(current, x);
				}
				// && move.size() != 0
				if (move != null) {

					moves.add(move);
				}
			}
		}

		return moves;
	}

}
