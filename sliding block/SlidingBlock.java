
public class SlidingBlock {

	int dx;
	int dx2;
	int dy;
	int dy2;

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDx2() {
		return dx2;
	}

	public void setDx2(int dx2) {
		this.dx2 = dx2;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}

	public int getDy2() {
		return dy2;
	}

	public void setDy2(int dy2) {
		this.dy2 = dy2;
	}

	public SlidingBlock(int dx, int dy, int dx2, int dy2) {

		this.dx = dx;
		this.dx2 = dx2;
		this.dy = dy;
		this.dy2 = dy2;
	}

	public static SlidingBlock getBlockDimension(State board, int peice) {
		// Dimensions of the piece
		int x1 = -1, y1 = -1;
		int x2 = -1, y2 = -1;
//find the coordinates and return the block
		for (int i = 0; i < board.x; i++) {

			for (int j = 0; j < board.y; j++) {

				if (board.st[i][j] == peice) {

					if (x1 == -1 && y1 == -1) {
						x1 = i;
						y1 = j;
						x2 = i;
						y2 = j;
					} else {

						x2 = i;
						y2 = j;
					}
				}
			}

			if (x1 == -1 && i == board.x - 1) {
				return null;
			}
		}

		return new SlidingBlock(x1, y1, x2, y2);

	}

}
