import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameBoard implements SlideGame {

	State gamestate = new State();
	FunctionState fs = new FunctionState();

	// load game state from a file
	public void loadGame(String filename) {
		int wide = 0, tall = 0;
		int array[][] = null;
		String csvFile = filename;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(csvFile));
			int count = -1;
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] data = line.split(cvsSplitBy);
				// System.out.println("data is"+data[0]+" "+data[1]+count);
				if (count == -1) {
					wide = Integer.parseInt(data[0]);
					tall = Integer.parseInt(data[1]);
					array = new int[tall][wide];
					count++;
				} else {
					for (int i = 0; i < data.length; i++) {

						array[count][i] = Integer.parseInt(data[i]);

					}
					count++;
				}

			}

			gamestate = new State(tall, wide, array);

			// gamestate.displayState();
			// System.out.println("initial state after normalization");
			gamestate.normalize();
			// gamestate.displayState();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean bfs(State abc) {
		ArrayList<State> explored = new ArrayList<State>();
		List<SearchNode> open = new ArrayList<SearchNode>();
		FunctionState fs = new FunctionState();

		SearchNode root = new SearchNode(abc);
		open.add(root);
		int count = 0;
		while (!open.isEmpty()) {
			SearchNode node = open.remove(0);
			// check if the board have the goal

			if (gamestate.isComplete(node.s)) {
				List<String> solution = getPath(node);
				node.s.displayState();

				System.out.println("Node Explored: " + count);
				return true;
			}
			// if the node game state is what we have been skip it
			if (isVisited(explored, node))
				continue;

			// add the node we have visited
			explored.add(node.s);
			count++;
			// this is to expand on the node of the children
			ArrayList<SearchNode> allnode = fs.getAllMoves(node);
			for (int i = 0; i < open.size(); i++)
				open.get(i).s.normalize();
			;
			if (allnode.size() != 0) {

				open.addAll(allnode);
			}

		}

		return false;
	}

	public boolean dfs(State abc) {
		ArrayList<State> explored = new ArrayList<State>();
		List<SearchNode> open = new ArrayList<SearchNode>();
		FunctionState fs = new FunctionState();

		SearchNode root = new SearchNode(abc);
		open.add(root);
		int count = 0;
		while (!open.isEmpty()) {
			SearchNode node = open.remove(0);

			// check if the game is completed
			if (gamestate.isComplete(node.s)) {
				List<String> solution = getPath(node);
				node.s.displayState();

				System.out.println("Node Explored: " + count);
				return true;
			}
			// check if you already visited the node
			if (isVisited(explored, node))
				continue;

			// add node to visited list
			explored.add(node.s);
			count++;
			// expand child nodes
			ArrayList<SearchNode> allnode = fs.getAllMoves(node);
			for (int i = 0; i < open.size(); i++)
				open.get(i).s.normalize();
			;
			if (allnode.size() != 0) {

				open.addAll(0, allnode);
			}

		}

		return false;
	}

	public State getGamestate() {
		return gamestate;
	}

	// random walks function
	public void randomWalks(int n) {
		if (n < 1) {

			System.out.println("no number of moves provided");
			this.gamestate.displayState();
			return;
		}
		for (int i = 0; i < n; i++) {
			if (gamestate.isComplete(gamestate)) {
				System.out.println("game completed");
				return;
			}
			ArrayList<ArrayList<Move>> allmoves = this.gamestate.getAllMoves();

			ArrayList<Move> moves = simplifyAllMoves(allmoves);
			Random rand = new Random();
			int rnumber = rand.nextInt(moves.size());
			// get random move apply it and normalize
			Move mv = moves.get(rnumber);
			gamestate.applyMove(mv);
			mv.displayMove();
			gamestate.normalize();
			gamestate.displayState();

		}

	}

	public ArrayList<Move> simplifyAllMoves(ArrayList<ArrayList<Move>> allmoves) {

		ArrayList<Move> moves = new ArrayList<Move>();
		for (int i = 0; i < allmoves.size(); i++) {
			ArrayList<Move> temp = allmoves.get(i);
			for (int j = 0; j < temp.size(); j++) {
				moves.add(temp.get(j));
			}

		}

		return moves;

	}

	public ArrayList<SearchNode> simplifyAllNodes(ArrayList<ArrayList<SearchNode>> allmoves) {

		ArrayList<SearchNode> moves = new ArrayList<SearchNode>();
		for (int i = 0; i < allmoves.size(); i++) {
			ArrayList<SearchNode> temp = allmoves.get(i);
			for (int j = 0; j < temp.size(); j++) {
				moves.add(temp.get(j));
			}

		}

		return moves;

	}

	// printing the path for success case
	public List<String> getPath(SearchNode current) {

		List<String> path = new ArrayList<>();

		while (current.parent != null) {
			path.add(0, current.piece + "," + current.action);

			current = current.parent;
		}
		for (int i = 0; i < path.size(); i++)
			System.out.println("( " + path.get(i) + " )");

		return path;

	}

	// check if the state is already visited
	protected boolean isVisited(ArrayList<State> visited, SearchNode node) {
		for (int i = 0; i < visited.size(); i++) {
			if (visited.get(i).isEqual(visited.get(i), node.s))
				return true;
		}

		return false;
	}

	// iterative deepening search algorithm
	public boolean ids(State a) {
		int d = 0;
		int count = 0;

		String result;
		while (true) {
			result = Dls(a, d, count);

			if (!(result.equals("cutoff"))) {
				return true;
			}
			d++;
		}

	}

	// depth limited search function
	private String Dls(State a, int depth, int count) {
		ArrayList<State> explored = new ArrayList<State>();
		SearchNode root = new SearchNode(a);
		root.depth = 0;
		return recursiveDls(root, explored, depth, count);
	}

	// recursive deep limited search function
	private String recursiveDls(SearchNode node, ArrayList<State> explored, int depth, int count) {

		boolean cutoff = false;
		if (gamestate.isComplete(node.s)) {

			List<String> solution = getPath(node);
			node.s.displayState();

			System.out.println("Node Explored: " + count);
			System.out.println("solution at depth" + node.depth);
			return "solution";
		} else if (node.depth == depth)
			return "cutoff";
		else if (isVisited(explored, node))
			return "fail";
		else {
			count++;
			ArrayList<SearchNode> allnode = fs.getAllMoves(node);
			for (int i = 0; i < allnode.size(); i++) {
				allnode.get(i).s.normalize();
				allnode.get(i).depth = allnode.get(i).parent.depth + 1;

				String result = recursiveDls(allnode.get(i), explored, depth, count);
				if (result.equals("cutoff"))
					cutoff = true;
				else if (!(result.equals("fail")))
					return "solution";

			}
		}
		if (cutoff)
			return "cutoff";
		else
			return "fail";

	}

}
