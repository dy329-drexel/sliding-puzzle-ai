
public class SearchNode {
	public State s;
	public SearchNode parent;
	public String action;
	public int piece;
	public int depth;
	
	public int getPiece() {
		return piece;
	}
	public void setPiece(int piece) {
		this.piece = piece;
	}
	public SearchNode(State aState,SearchNode aNode,String aAction){
		s=aState;
		parent=aNode;
		action=aAction;
		
		
	}
	public SearchNode(State aState){
		s=aState;
		parent=null;
		action=null;
		
		
	}
	public SearchNode(State applyMovesCloning, SearchNode sn, String direction, int aPiece) {
		s=applyMovesCloning;
		parent=sn;
		action=direction;
		piece=aPiece;
	}
	
	//check if objects are equal
	public boolean equals(Object o){
		if(!(o instanceof SearchNode))
			return false;
		SearchNode sn=(SearchNode)o;
		return s.isEqual(sn.s, s);
		
		
	}
	
	public String toString(){
		
		return "["+s+","+action+"]";
	}
	
	

}
