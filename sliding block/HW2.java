
public class HW2 {

	public static void main(String args[]) {

		GameBoard gb = new GameBoard();
		if (args.length < 2) {
			System.out.println("please provide proper number of arguments");
			System.exit(0);
		} else if(args.length==2) {
			gb.loadGame(args[0]);
			
			switch(args[1].toLowerCase()){
			case "bfs":{
				
				long sTime = System.currentTimeMillis();
				gb.bfs(gb.gamestate);
				long eTime = System.currentTimeMillis();
				System.out.println("Time for search "+ + (eTime - sTime) + " milli seconds");
				break;
				
			}
			case "dfs": {
				long sTime = System.currentTimeMillis();
				gb.dfs(gb.gamestate);
				long eTime = System.currentTimeMillis();
				System.out.println("Time for search "+ + (eTime - sTime) + " milli seconds");break;
			}
			case "ids":{
				long sTime = System.currentTimeMillis();
				gb.ids(gb.gamestate); 
				long eTime = System.currentTimeMillis();
				System.out.println("Time for search "+ + (eTime - sTime) + " milli seconds");break;
			}
			default:System.out.println("specify search");
		}
			
			
			

		}
	}
}
